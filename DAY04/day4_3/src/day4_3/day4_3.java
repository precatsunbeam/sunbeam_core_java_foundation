package day4_3;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

public class day4_3 {

	public static void main(String[] args) 
	{
		ArrayList<String> list=new ArrayList<String>();
		//create a array list of String
		list.add("Akshita");
		list.add("Sunbeam");
		list.add("Infotech");
		//list.add(50); // ArrayList<String> is not applicable for the arguments (int)
		list.add("Pune");
		System.out.println(list);
		
		list.remove(3); //index 3 
		System.out.println("After Removing Element : "+list);
		System.out.println("Printing data using Iterator :");
		//printing the list element 
		// Traversing through Iterator 
		Iterator itr=list.iterator(); // Iterator (cursor)
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		System.out.println("Printing data using for :");
		for(int i=0;i<list.size();i++)
		{
			System.out.print("\t"+i);
			System.out.print("\t"+list.get(i));
			
		}
		System.out.println("Printing data using for :");
		for(String ele:list)  
		{
			System.out.print("\t"+ele);
		}
		
		
		Collections.sort(list);
		System.out.println("\n After Sorting "+list);
		
		System.out.println("Set Example");
		list.set(0, "Aks");
		System.out.println("\n setting new element  "+list);
		
		
		
		
		
		
		
		

	}

}
